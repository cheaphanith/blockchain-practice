export FABRIC_CFG_PATH=${PWD}/config/
# configtxgen -inspectChannelCreateTx ./artifact/mychannel.tx
configtxgen -outputAnchorPeersUpdate ./artifact/Org1Anchor.tx -profile TwoOrgsApplicationGenesis -channelID mychannel -asOrg Org1MSP
configtxgen -outputAnchorPeersUpdate ./artifact/Org2Anchor.tx -profile TwoOrgsApplicationGenesis -channelID mychannel -asOrg Org1MSP

